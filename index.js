// console.log("Siesta Time!");

// Array Methods

// Js build in functions and methods for arrays

// [Section] Mutator Methods
let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

console.log(fruits);

console.log("------------------------------------------------------------------------");

// push() - Adds at the end and returns array length

console.log("Current Array fruits[]: ");
console.log(fruits);

console.log("Mutated Array fruits[]: " + fruits.push("Mango"));
console.log(fruits);

// multiple push()

console.log("Mutated Array fruits[]: " + fruits.push("Avocado", "Guava"));
console.log(fruits);

console.log("------------------------------------------------------------------------");

// pop() - removes last element and returns the said element

console.log("Current Array fruits[]: ");
console.log(fruits);

console.log("Mutated Array fruits[]: Removed Fruit -> " + fruits.pop());
console.log(fruits);
fruits.pop()

console.log("------------------------------------------------------------------------");

// unshift() - add at the beginning of the array and returns the array lenght

console.log("Current Array fruits[]: ");
console.log(fruits);

console.log("Mutated Array fruits[]: " + fruits.unshift("Lime", "Banana"));
console.log(fruits);

console.log("------------------------------------------------------------------------");

//shift() removes element at the start and returns the said element

console.log("Current Array fruits[]: ");
console.log(fruits);

console.log("Mutated Array fruits[]: Removed Fruit -> " + fruits.shift());
console.log(fruits);

console.log("------------------------------------------------------------------------");

//splice() - Removes a specified element and insert an element. returns the removed elements
//splice(index to add, how many to remove after index stated to add, what to add)

console.log("Current Array fruits[]:");
console.log(fruits);

console.log("Mutated Array fruits[]: Removed Fruit -> " + fruits.splice(1, 1, "Lime"));
console.log(fruits);

console.log("Mutated Array fruits[]: Removed Fruit -> " + fruits.splice(3, 1));
console.log(fruits);

fruits.splice(3, 0, "Durian", "Santol");
console.log("Mutated Array fruits[]:");
console.log(fruits);

console.log("------------------------------------------------------------------------");

//sort() - Rearranges the array elements in alphanumerical order, returns array

console.log("Current Array fruits[]:");
console.log(fruits);

fruits.sort();
console.log("Mutated Array fruits[]:");
console.log(fruits);

console.log("------------------------------------------------------------------------");

//reverse() - reverses the Array, returns array

console.log("Current Array fruits[]:");
console.log(fruits);

fruits.reverse();
console.log("Mutated Array fruits[]:");
console.log(fruits);

// [Section] Non-mutator Methods - does not modify the array.

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE', 'PH'];

// indexOf() - returns index of matching element in array returns -1 if no match,

	//Syntax: arrayName.indexOf(searchValue, startingIndex);

console.log(countries);
console.log(countries.indexOf('PH'));
console.log(countries.indexOf('BR'));
console.log(countries.indexOf('PH', 2));

console.log("------------------------------------------------------------------------");

// lastIndexOf() - indexOf but backwards

console.log(countries.lastIndexOf('PH'));

console.log("------------------------------------------------------------------------");

// slice() - returns a portion of an array and returns a new array.
	// arrayName.slice(startingIndex, endingIndex);

 console.log(countries.slice(2));
 console.log(countries.slice(2, 5));

 // toString() - returns array as strings separated by commas

 console.log(countries.toString());

 // concat() combines array and returned the combined arrays

 let tasksArrayA = ["drink HTML", "eat javascript"];
 let tasksArrayB = ["inhale CSS", "breath SASS"];
 let tasksArrayC = ["get git", "be node"];

 console.log(tasksArrayA.concat(tasksArrayB));
 console.log(tasksArrayA.concat(tasksArrayB, tasksArrayC));
 console.log(tasksArrayA.concat(tasksArrayB, 'smell express', 'throw react'));

 let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);

 // join() - returns an array as string with specified separator strings

 let users = ["John", "Jane", "Joe", "Robert"];

 console.log(users);
 console.log(users.join(' '));
 console.log(users.join(' - '));

 //forEach()

 console.log(allTasks);

 allTasks.forEach(function(task){
 	console.log(task);
 })

 let filteredTasks = [];

 allTasks.forEach(function(task){
 	if (task.length > 10){
 		filteredTasks.push(task);
 	}
 });

 console.log(filteredTasks);


	 // map() iterates on each element and returns new array with different values depending on the result of the function's operation.
		/* -Syntax:
		  let/const resultArray = arrayName.map(function(elements){
		statements;
		return;
	}) 
	*/

 let numbers = [1, 2, 3, 4, 5];

 let numberMap = numbers.map(function(number){
 	return number*number;
 });

 console.log(numberMap);

 // every()

  let allValid = numbers.every(function(number){
 	return(number > 1);
 });

 console.log(allValid);

 // some()

 let someValid = numbers.some(function(number){
 	return(number > 4);
 });

 console.log(someValid);

 // filter()

 console.log(numbers);

 let filterValid = numbers.filter(function(number){
 	return (number < 3);
 });
 console.log(filterValid);

 let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

 let producttFound1 = products.includes('Mouse');
 console.log(productFound1);

 // reduce()

 console.log(numbers);
 let total = numbers.reduce(function(x,y){
 	return x + y;
 })